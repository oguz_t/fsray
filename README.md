# FSRay

__All relevant source code is stored inside *inc/fsray.h* and *src/main.cpp*__

## Usage:
`./fsray -<n> <obj_file>`\
`<n> - mode numer`

You could also Drag & Drop .OBJ file directly onto executable and everything should load up properly. In this case, default mode is used.
You could also Drag & Drop .OBJ file onto batch files from *dat* folder to open object with specific mode.

Be aware that in current state, camera and light share look vectors with each other! This in turn means that when you change eye positions around, you would always face same direction as you did before.

## Modes:
Mode | Settings
----|-------
0|  320×180 resolution, 4×4 scaling
1|  427×240 resolution, 3×3 scaling (default)
2|  640×360 resolution, 2×2 scaling
3| 1280×720 resolution, 1×1 scaling


## Controls:
Key | Action
----|-------
WASD | move around
Q/E | go down/up
↑←↓→ | look around
SHIFT | movement speed boost (while held)
CTRL | look speed boost (while held) 
C | change eye position to camera position
L | change eye position to light position
N | toggle visualize normals
M | toggle multiple objects
Y | disable lighting
T | toggle rendering of abstract objects (camera/light)
1/2/3/4/5 | change color values (accept changes with ENTER, discard changes with ESCAPE)
7/8/9 | change transformation values (accept changes with ENTER, discard changes with ESCAPE)
O/P | change pixel division level (default is 2, so 4 rays per pixel)
NP+/NP- | modify FOV
