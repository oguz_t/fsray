# Initialize project
cmake_minimum_required(VERSION 3.13)
project(fsray_deps C)

# Add FSRay library dependencies
# FSLib (core library)
set(FSL_LIB_TYPE "OBJECT" CACHE STRING "Compile FSLib as: (STATIC | SHARED | OBJECT) [def. STATIC]")
option(FSL_BUILD_TESTS "Build tests" OFF)
add_subdirectory(fslib)

# Create include list for dependencies
set(FSR_DEP_INCLUDES
  ${FSL_INCLUDES}
  ${OPENGL_INCLUDE_DIR}
  CACHE INTERNAL "FSR_DEP_INCLUDES"
)