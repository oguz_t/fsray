#include <fslib/math/fsl_math.h>
#include <fslib/filesystem/fsl_filesystem.h>
#include <printf.h>

#include <vector>
#include <unordered_set>

typedef string_const_t cstr_t;

// structs for vec4_t unordered_set
struct vec4_hashfunc_t {
  size_t operator()(const vec4_t& vec) const noexcept {
    fsl_flt32_t fi;
    fi.f = (vec.x * vec.y) - (vec.y * vec.z) + (vec.z * vec.w);
    return static_cast<size_t>(fi.i);
  }
};
struct vec4_cmpfunc_t {
  bool operator()(const vec4_t& lhs, const vec4_t& rhs) const noexcept {
    return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z) && (lhs.w == rhs.w);
  }
};

struct material_t {
  vec4_t  ambient;
  vec4_t  diffuse;
  vec4_t  specular;
  flt32_t shininess;
};

struct aabb_t {
  vec4_t min;
  vec4_t max;
};

struct tri_t {
  vec4_t  a;
  vec4_t  b;
  vec4_t  c;
  vec4_t  an;
  vec4_t  bn;
  vec4_t  cn;
  vec4_t  e1;
  vec4_t  e2;
  vec4_t  n;
  vec4_t  col;
  flt32_t d11;
  flt32_t d12;
  flt32_t d22;
  flt32_t invdenom;

  void Init(vec4_t inA, vec4_t inB, vec4_t inC, vec4_t inAn, vec4_t inBn, vec4_t inCn) {
    a = inA;
    b = inB;
    c = inC;
    an = inAn;
    bn = inBn;
    cn = inCn;
    e1 = FSL_Vec4_Sub(b, a);
    e2 = FSL_Vec4_Sub(c, a);
    n = FSL_Vec4_Normalize(FSL_Vec4_Cross(e1, e2));
    col = FSL_Vec4_Mul(FSL_Vec4_Add(n, FSL_Vec4_Set1(1.0f)), FSL_Vec4_Set(0.5f, 0.5f, 0.5f, 1.0f));
    d11 = FSL_Vec4_Dot(e1, e1);
    d12 = FSL_Vec4_Dot(e1, e2);
    d22 = FSL_Vec4_Dot(e2, e2);
    invdenom = 1.0f / ((d11 * d22) - (d12 * d12));
  }

  tri_t Transform(mat4_t model, mat4_t modelNorm) const {
    tri_t res;
    res.a = FSL_Mat4_Mul4(model, a);
    res.b = FSL_Mat4_Mul4(model, b);
    res.c = FSL_Mat4_Mul4(model, c);
    res.an = FSL_Vec4_Normalize(FSL_Mat4_Mul4(modelNorm, an));
    res.bn = FSL_Vec4_Normalize(FSL_Mat4_Mul4(modelNorm, bn));
    res.cn = FSL_Vec4_Normalize(FSL_Mat4_Mul4(modelNorm, cn));
    res.e1 = FSL_Vec4_Sub(res.b, res.a);
    res.e2 = FSL_Vec4_Sub(res.c, res.a);
    res.n = FSL_Vec4_Normalize(FSL_Mat4_Mul4(modelNorm, n));
    res.col = FSL_Vec4_Mul(FSL_Vec4_Add(res.n, FSL_Vec4_Set1(1.0f)), FSL_Vec4_Set(0.5f, 0.5f, 0.5f, 1.0f));
    res.d11 = FSL_Vec4_Dot(res.e1, res.e1);
    res.d12 = FSL_Vec4_Dot(res.e1, res.e2);
    res.d22 = FSL_Vec4_Dot(res.e2, res.e2);
    res.invdenom = 1.0f / ((res.d11 * res.d22) - (res.d12 * res.d12));
    return res;
  }
};

struct ray_t {
  vec4_t base;
  vec4_t dir;
};

struct sphere_t {
  vec4_t  center;
  flt32_t radius;
};

struct mesh_t {
  public:
    vec4_t*   verts       = nullptr;
    vec4_t*   vert_norms  = nullptr;
    umax_t    verts_count = 0;
    uint32_t* inds        = nullptr;
    umax_t    inds_count  = 0;
    vec4_t*   face_norms  = nullptr;
    umax_t    face_count  = 0;
    aabb_t    hullAABB    = { };
    bool      ccw_wind    = true;
    bool      dynamic     = true;

    ~mesh_t() {
      if (!dynamic) {
        return;
      }
      if (verts) {
        FSL_Mem_Free(verts);
      }
      if (vert_norms) {
        FSL_Mem_Free(vert_norms);
      }
      if (inds) {
        FSL_Mem_Free(inds);
      }
      if (face_norms) {
        FSL_Mem_Free(face_norms);
      }
    }

    int32_t LoadOBJ(const char* objPath) {
      fsl_file_mem_t* objFile = FSL_File_LoadToMem(objPath);
      if (!objFile) {
        return 1;
      }
      fsl_objtok_t objTok = { };
      FSL_OBJTOK_SetData(&objTok, reinterpret_cast<char*>(objFile->data));

      // init AABB hull coords
      hullAABB.min = FSL_Vec4_Set( INFINITY,  INFINITY,  INFINITY, 1.0f);
      hullAABB.max = FSL_Vec4_Set(-INFINITY, -INFINITY, -INFINITY, 1.0f);

      vec4_t                vertArg = FSL_VEC4_POS_W;
      vec4_t                normArg = FSL_VEC4_ZERO;
      std::vector<vec4_t>   inVerts;
      std::vector<vec4_t>   inVertNorms;
      std::vector<uint32_t> inInds;
      std::vector<vec4_t>   inFaceNorms;
      fsl_obj_token_t* objTokToken = FSL_OBJTOK_GetNextToken(&objTok);
      while (objTokToken) {
        switch (objTokToken->type) {
          case OTT_TYPE_VERTEX_POSITION:
            vertArg.x = strtof(objTokToken->token, &objTokToken->token);
            vertArg.y = strtof(objTokToken->token, &objTokToken->token);
            vertArg.z = strtof(objTokToken->token, &objTokToken->token);
            inVerts.push_back(vertArg);
            // update hull
            hullAABB.min = FSL_Vec4_Min(hullAABB.min, vertArg);
            hullAABB.max = FSL_Vec4_Max(hullAABB.max, vertArg);
            break;
          case OTT_TYPE_VERTEX_NORMAL:
            normArg.x = strtof(objTokToken->token, &objTokToken->token);
            normArg.y = strtof(objTokToken->token, &objTokToken->token);
            normArg.z = strtof(objTokToken->token, &objTokToken->token);
            inVertNorms.push_back(normArg);
            break;
          case OTT_TYPE_FACE:
            uint32_t a = strtoul(objTokToken->token, &objTokToken->token, 10) - 1;
            inInds.push_back(a);
            uint32_t b = strtoul(objTokToken->token, &objTokToken->token, 10) - 1;
            inInds.push_back(b);
            uint32_t c = strtoul(objTokToken->token, &objTokToken->token, 10) - 1;
            inInds.push_back(c);
            // calc face norm
            normArg = FSL_Vec4_Normalize(FSL_Vec4_Cross(FSL_Vec4_Sub(inVerts[b], inVerts[a]), FSL_Vec4_Sub(inVerts[c], inVerts[a])));
            inFaceNorms.push_back(normArg);
            break;
        }
        objTokToken = FSL_OBJTOK_GetNextToken(&objTok);
      }


      verts_count = inVerts.size();
      verts = reinterpret_cast<vec4_t*>(FSL_Mem_Alloc(verts_count * sizeof(vec4_t)));
      FSL_Mem_Copy(verts, &inVerts[0], verts_count * sizeof(vec4_t));
      vert_norms = reinterpret_cast<vec4_t*>(FSL_Mem_Alloc(verts_count * sizeof(vec4_t)));
      FSL_Mem_Set(vert_norms, 0, verts_count * sizeof(vec4_t));
      inds_count = inInds.size();
      inds = reinterpret_cast<uint32_t*>(FSL_Mem_Alloc(inds_count * sizeof(uint32_t)));
      FSL_Mem_Copy(inds, &inInds[0], inds_count * sizeof(uint32_t));
      face_count = (inds_count / 3);
      face_norms = reinterpret_cast<vec4_t*>(FSL_Mem_Alloc(face_count * sizeof(vec4_t)));
      FSL_Mem_Copy(face_norms, &inFaceNorms[0], face_count * sizeof(vec4_t));
      ccw_wind = true;

      // TODO - if obj has vertex normals, use them!
      //if (inVertNorms.empty()) {
        std::unordered_set<vec4_t, vec4_hashfunc_t, vec4_cmpfunc_t>* vertNorms = new std::unordered_set<vec4_t, vec4_hashfunc_t, vec4_cmpfunc_t>[verts_count];
        for (size_t i = 0; i < inds_count; ++i){
          vertNorms[inds[i]].insert(face_norms[i/3]);
        }
        #pragma omp parallel for schedule(dynamic)
        for (max_t i = 0; i < (max_t)verts_count; ++i) {
          for (vec4_t norm: vertNorms[i]) {
            vert_norms[i] = FSL_Vec4_Add(vert_norms[i], norm);
          }
          vert_norms[i] = FSL_Vec4_Normalize(vert_norms[i]);
        }
        delete[] vertNorms;
      //}
      //else {
        //FSL_Mem_Copy(vert_norms, &inVertNorms[0], verts_count * sizeof(vec4_t));
      //}

      FSL_File_FreeFromMem(objFile);
      return 0;
    }

    void UpdateNormals() {
      std::unordered_set<vec4_t, vec4_hashfunc_t, vec4_cmpfunc_t>* vertNorms = new std::unordered_set<vec4_t, vec4_hashfunc_t, vec4_cmpfunc_t>[verts_count];
      #pragma omp parallel for schedule(dynamic)
      for (max_t i = 0; i < (max_t)inds_count; i += 3){
        size_t faceI = i / 3;
        uint32_t ai = inds[i + 0];
        uint32_t bi;
        uint32_t ci;
        if (ccw_wind) {
          bi = inds[i + 1];
          ci = inds[i + 2];
        }
        else {
          bi = inds[i + 2];
          ci = inds[i + 1];
        }
        vec4_t fn = FSL_Vec4_Normalize(FSL_Vec4_Cross(FSL_Vec4_Sub(verts[bi], verts[ai]), FSL_Vec4_Sub(verts[ci], verts[ai])));
        face_norms[faceI] = fn;
        #pragma omp critical
        {
          vertNorms[ai].insert(fn);
          vertNorms[bi].insert(fn);
          vertNorms[ci].insert(fn);
        }
      }
      #pragma omp parallel for schedule(dynamic)
      for (max_t i = 0; i < (max_t)verts_count; ++i) {
        vert_norms[i] = FSL_Vec4_SetZero();
        for (vec4_t norm: vertNorms[i]) {
          vert_norms[i] = FSL_Vec4_Add(vert_norms[i], norm);
        }
        vert_norms[i] = FSL_Vec4_Normalize(vert_norms[i]);
      }
      delete[] vertNorms;
    }
};

struct transform_t {
  vec4_t position;
  vec4_t rotation;
  vec4_t scale;
};

struct fsr_obj_t {
  aabb_t      hull;       //= { };
  tri_t*      tris;       //= nullptr;
  size_t      trisCount;  //= 0;
  transform_t transform;  //= { };
  material_t  material;   //= { };
};

// initialize cube mesh data
FSL_GLOBAL_PERSIST vec4_t q_cube_verts[24] = {
  // right
  FSL_VEC4_INIT( 1.0f, -1.0f, -1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT( 1.0f, -1.0f,  1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT( 1.0f,  1.0f,  1.0f, 1.0f),    // top right
  FSL_VEC4_INIT( 1.0f,  1.0f, -1.0f, 1.0f),    // top left
  // left
  FSL_VEC4_INIT(-1.0f, -1.0f,  1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT(-1.0f, -1.0f, -1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT(-1.0f,  1.0f, -1.0f, 1.0f),    // top right
  FSL_VEC4_INIT(-1.0f,  1.0f,  1.0f, 1.0f),    // top left
  // top
  FSL_VEC4_INIT(-1.0f,  1.0f, -1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT( 1.0f,  1.0f, -1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT( 1.0f,  1.0f,  1.0f, 1.0f),    // top right
  FSL_VEC4_INIT(-1.0f,  1.0f,  1.0f, 1.0f),    // top left
  // bottom
  FSL_VEC4_INIT(-1.0f, -1.0f,  1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT( 1.0f, -1.0f,  1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT( 1.0f, -1.0f, -1.0f, 1.0f),    // top right
  FSL_VEC4_INIT(-1.0f, -1.0f, -1.0f, 1.0f),    // top left
  // back
  FSL_VEC4_INIT( 1.0f, -1.0f,  1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT(-1.0f, -1.0f,  1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT(-1.0f,  1.0f,  1.0f, 1.0f),    // top right
  FSL_VEC4_INIT( 1.0f,  1.0f,  1.0f, 1.0f),    // top left
  // front
  FSL_VEC4_INIT(-1.0f, -1.0f, -1.0f, 1.0f),    // bottom left
  FSL_VEC4_INIT( 1.0f, -1.0f, -1.0f, 1.0f),    // bottom right
  FSL_VEC4_INIT( 1.0f,  1.0f, -1.0f, 1.0f),    // top right
  FSL_VEC4_INIT(-1.0f,  1.0f, -1.0f, 1.0f),    // top left
};
FSL_GLOBAL_PERSIST vec4_t q_cube_vert_norms[24] = {
  // initialized during runtime
};
FSL_GLOBAL_PERSIST uint32_t q_cube_inds[36] = {
   0,  1,  3,  1,  2,  3,    // right face
   4,  5,  7,  5,  6,  7,    // left face
   8,  9, 11,  9, 10, 11,    // top face
  12, 13, 15, 13, 14, 15,    // bottom face
  16, 17, 19, 17, 18, 19,    // back face
  20, 21, 23, 21, 22, 23     // front face
};
FSL_GLOBAL_PERSIST vec4_t q_cube_face_norms[12] = {
  // initialized during runtime
};
// initialize cube mesh
FSL_GLOBAL_PERSIST mesh_t Q_MESH_CUBE = {
  q_cube_verts,
  q_cube_vert_norms,
  24,
  q_cube_inds,
  36,
  q_cube_face_norms,
  12,
  {
    FSL_VEC4_INIT(-1.0f, -1.0f, -1.0f, 1.0f),
    FSL_VEC4_INIT( 1.0f,  1.0f,  1.0f, 1.0f)
  },
  false,
  false
};

FSL_GLOBAL_PERSIST char dummyPath[1] = { };
FSL_GLOBAL_PERSIST char* modelPath = dummyPath;

// get normal vector for point in tri
FSL_INLINE_INTERNAL vec4_t NormalAtPointInTri(const tri_t& tri, vec4_t p) {
  vec4_t vp = FSL_Vec4_Sub(p, tri.a);
  flt32_t dp1 = FSL_Vec4_Dot(vp, tri.e1);
  flt32_t dp2 = FSL_Vec4_Dot(vp, tri.e2);
  flt32_t v = ((tri.d22 * dp1) - (tri.d12 * dp2)) * tri.invdenom;
  flt32_t w = ((tri.d11 * dp2) - (tri.d12 * dp1)) * tri.invdenom;
  flt32_t u = 1.0f - v - w;
  return FSL_Vec4_Normalize(FSL_Vec4_Add(FSL_Vec4_Add(FSL_Vec4_Mul1(tri.an, u), FSL_Vec4_Mul1(tri.bn, v)), FSL_Vec4_Mul1(tri.cn, w)));
}

// intersect ray with aabb
FSL_INLINE_INTERNAL flt32_t IntersectAABB(aabb_t box, ray_t ray) {
  // calc min/max values
  //
  vec4_t t1 = FSL_Vec4_Div(FSL_Vec4_Sub(box.min, ray.base), ray.dir);
  vec4_t t2 = FSL_Vec4_Div(FSL_Vec4_Sub(box.max, ray.base), ray.dir);
  // extract max from min values
  flt32_t tmaxmin = FSL_PF32_MaxFrom3(FSL_Vec4_Min(t1, t2).xmm);
  // extract min from max values
  flt32_t tminmax = FSL_PF32_MinFrom3(FSL_Vec4_Max(t1, t2).xmm);
  // test for hit
  flt32_t tminmax_test = FSL_MAX(tmaxmin, 0.0f);
  if (tminmax >= tminmax_test) {
    return tmaxmin;
  }
  return INFINITY;
}

// intersect ray with sphere
FSL_INLINE_INTERNAL flt32_t IntersectSphere(sphere_t sphere, ray_t ray) {
#if 1
  vec4_t oc = FSL_Vec4_Sub(ray.base, sphere.center);
  flt32_t a = FSL_Vec4_Dot(ray.dir, ray.dir);  // not always 1 due to normalization errors, must be included...
  flt32_t b = FSL_Vec4_Dot(oc, ray.dir);
  flt32_t c = FSL_Vec4_Dot(oc, oc) - (sphere.radius * sphere.radius);
  flt32_t D = (b * b) - (a * c);
  //flt32_t D = (b * b) - c;
  if(D > 0.0f){
    flt32_t t = -b - FSL_F32_Sqrt(D);
    if (t > 0.0f) {
      return t / a;
      //return t;
    }
  }
  return INFINITY;
#else // if ray.dir would be always length of 1, this should perform better...
  vec4_t c = FSL_Vec4_Sub(sphere.center, ray.base);
  flt32_t v = FSL_Vec4_Dot(c, ray.dir);
  if (v < 0.0f) {
    return INFINITY;
  }
  flt32_t r2 = sphere.radius * sphere.radius;
  flt32_t v2 = v * v;
  flt32_t c2 = FSL_Vec4_Dot(c, c);
  flt32_t b2 = c2 - v2;
  if (r2 > b2) {
    return v - FSL_F32_Sqrt(r2 - b2);
  }
  return INFINITY;
#endif
}

// intersect ray with tri
FSL_INLINE_INTERNAL flt32_t IntersectTriangle(const tri_t& tri, ray_t ray) {
  vec4_t pvec = FSL_Vec4_Cross(ray.dir, tri.e2);
  flt32_t det = FSL_Vec4_Dot(tri.e1, pvec);
  if (det < FSL_F32_EPSILON) {
    return INFINITY;
  }
  vec4_t tvec = FSL_Vec4_Sub(ray.base, tri.a);
  flt32_t u = FSL_Vec4_Dot(tvec, pvec);
  if (u < 0.0f) {
    return INFINITY;
  }
  flt32_t uvb = det - u; // barycentric check
  if (uvb < 0.0f) {
    return INFINITY;
  }
  vec4_t qvec = FSL_Vec4_Cross(tvec, tri.e1);
  flt32_t v = FSL_Vec4_Dot(ray.dir, qvec);
  if (v < 0.0f) {
    return INFINITY;
  }
  if (uvb < v) {
    return INFINITY;
  }
  flt32_t t = FSL_Vec4_Dot(tri.e2, qvec);
  if (t > 0.0f) {
    return t / det;
  }
  return INFINITY;
}