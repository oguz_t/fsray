#define OLC_PGE_APPLICATION
#include <olcPixelGameEngine.h>
#include <olcPGEX_UI.h>

#include <fsray.h>

// TODO:
//  - render to file

class FSRay : public olc::PixelGameEngine {
  public:
    FSRay() {
      sAppName = "FSRay";
    }

	  // initialization here
    bool OnUserCreate() override {
      // initialize memory for thread
      FSL_Mem_Thread_Init();
      // initialize members
      scrWidth   = static_cast<size_t>(ScreenWidth());
      scrHeight  = static_cast<size_t>(ScreenHeight());
      scrWidthF  = static_cast<flt32_t>(scrWidth);
      scrHeightF = static_cast<flt32_t>(scrHeight);
      fov = 45.0f;
      const flt32_t fov_y = FLT32_DEG2RAD(fov) / 2.0f;
      scale = FSL_F32_SinRad(fov_y) / FSL_F32_CosRad(fov_y);
      aspect = scrWidthF / scrHeightF;
      eyePos = &camera.center;
      // load mesh data
      mesh_t mesh = { };
      if (mesh.LoadOBJ(modelPath)) {
        mesh = Q_MESH_CUBE;
        mesh.UpdateNormals();
      }
      // calculate transformation to fit all object vertices into unit cube
      objHull = mesh.hullAABB;
      vec4_t objHalfExtent = FSL_Vec4_Mul1(FSL_Vec4_Sub(objHull.max, objHull.min), 0.5f);
      vec4_t objHalfExtentN = FSL_Vec4_Normalize(objHalfExtent);
      //vec4_t objCenter = FSL_Vec4_Add(objHull.min, objHalfExtent);
      vec4_t objToOrigin = FSL_Vec4_Sub(FSL_Vec4_Set(0.0f, 0.0f, 0.0f, 1.0f), objHull.min);
      objToOrigin = FSL_Vec4_Sub(objToOrigin, objHalfExtent);
      vec4_t objScaleOrigin = FSL_Vec4_DivZero(objHalfExtentN, objHalfExtent);
      mat4_t objTransformScale = FSL_Mat4_Scale(objScaleOrigin);
      vec4_t objHalfExtentN1 = FSL_Vec4_Add(objHalfExtentN, FSL_VEC4_POS_W);
      objHull.min = FSL_Vec4_Mul(Q_MESH_CUBE.hullAABB.min, objHalfExtentN1);
      objHull.max = FSL_Vec4_Mul(Q_MESH_CUBE.hullAABB.max, objHalfExtentN1);
      mat4_t objTransform = FSL_Mat4_Mul(objTransformScale, FSL_Mat4_Translation(objToOrigin));
      // transform object vertices into unit cube
      #pragma omp parallel for schedule(dynamic)
      for (max_t i = 0; i < (max_t)mesh.verts_count; ++i) {
        mesh.verts[i] = FSL_Mat4_Mul4(objTransform, mesh.verts[i]);
      }
      // parse object data
      objTrisCount = mesh.inds_count / 3;
      objTris = reinterpret_cast<tri_t*>(FSL_Mem_Alloc(objTrisCount * sizeof(tri_t)));

      for (size_t i = 0; i < objsCount; ++i) {
        objs[i].tris = reinterpret_cast<tri_t*>(FSL_Mem_Alloc(objTrisCount * sizeof(tri_t)));
        objs[i].trisCount = objTrisCount;
      }
      #pragma omp parallel for schedule(dynamic)
      for (max_t i = 0; i < (max_t)mesh.inds_count; i += 3) {
        tri_t& tri = objTris[i / 3];
        uint32_t ai = mesh.inds[i + 0];
        uint32_t bi;
        uint32_t ci;
        if (mesh.ccw_wind) {
          bi = mesh.inds[i + 1];
          ci = mesh.inds[i + 2];
        }
        else {
          bi = mesh.inds[i + 2];
          ci = mesh.inds[i + 1];
        }
        tri.Init(
          mesh.verts[ai], mesh.verts[bi], mesh.verts[ci],
          mesh.vert_norms[ai], mesh.vert_norms[bi], mesh.vert_norms[ci]
        );
      }
      // initialize UI
      ui.addTextField(20, 25, 10); // id: 0
      ui.addTextField(20, 45, 10); // id: 1
      ui.addTextField(20, 65, 10); // id: 2
      //
      return true;
    }

    // deinitialization here
    bool OnUserDestroy() override {
      if (objTris) {
        FSL_Mem_Free(objTris);
      }
      for (size_t i = 0; i < objsCount; ++i) {
        if (objs[i].tris) {
          FSL_Mem_Free(objs[i].tris);
        }
      }
      FSL_Mem_Thread_DeInit();
      return true;
    }

    // called once per frame
    bool OnUserUpdate(flt32_t fElapsedTime) override {
      // update input events
      UpdateInput(fElapsedTime);
      // update UI events
      UpdateUI(fElapsedTime);
      // transform object
      TransformObject();
      // update view/primary ray base
      vec4_t rayBase = *eyePos;
      mat4_t invView = FSL_Mat4_InvView(rayBase, lookRight, lookUp, lookForward);
      // object intersection function
      auto objIntersect = [&](fsr_obj_t& obj, ray_t& ray, flt32_t& tnearS, vec4_t& col) {
        vec4_t  matAmb   = FSL_Vec4_Mul(material.ambient, obj.material.ambient);
        vec4_t  matDiff  = FSL_Vec4_Mul(material.diffuse, obj.material.diffuse);
        vec4_t  matSpec  = FSL_Vec4_Mul(material.specular, obj.material.specular);
        flt32_t matShine = material.shininess * obj.material.shininess;
        // check if ray intersects AABB hull of object (if not, we don't need to compute anything)
        if (IntersectAABB(obj.hull, ray) < tnearS) {
          size_t triI = 0;
          flt32_t tnear = tnearS; // closest intersection, set to INFINITY to start with
          for (size_t i = 0; i < objTrisCount; ++i) {
            flt32_t t = IntersectTriangle(obj.tris[i], ray);
            if (t < tnear) {
              tnear = t;
              triI = i;
            }
          }
          // calc lighting and color (if intersection found and object in front)
          if (tnear < tnearS) {
            // save tnearS
            tnearS = tnear;
            // reset color
            col = FSL_Vec4_SetZero();
            // generate secondary ray
            vec4_t point = FSL_Vec4_Add(rayBase, FSL_Vec4_Mul1(ray.dir, tnear));
            vec4_t point_norm = NormalAtPointInTri(obj.tris[triI], point);
            vec4_t light_dir = FSL_Vec4_Sub(point, light.center); // intentionally inverted operands
            flt32_t light_dist = FSL_Vec4_Length(light_dir);
            vec4_t light_ray = FSL_Vec4_Normalize(light_dir);
            flt32_t lambert = -FSL_Vec4_Dot(point_norm, light_ray); // inverse result as light dir is inverted
            lambert = FSL_MAX(lambert, 0.0f);
            ray_t rayS = { light.center, light_ray }; // light ray is inverted already!
            bool has_obstacle = false;
            if (lambert > 0.0f) {
              // test for obstacles (if shadows enabled)
              if (lightShadows) {
                if (objMultiple) {
                  for (size_t i = 0; i < objsCount; ++i) {
                    fsr_obj_t &objT = objs[i];
                    if ((&objT != &obj) && /*!has_obstacle &&*/ (IntersectAABB(objT.hull, rayS) < light_dist)) {
                      for (size_t j = 0; !has_obstacle && (j < objTrisCount); ++j) {
                        flt32_t t = IntersectTriangle(objT.tris[j], rayS);
                        has_obstacle = (t < light_dist);
                      }
                    }
                  }
                }
                for (size_t j = 0; !has_obstacle && (j < triI); ++j) {
                  flt32_t t = IntersectTriangle(obj.tris[j], rayS);
                  has_obstacle = (t < light_dist);
                }
                for (size_t j = triI + 1; !has_obstacle && (j < objTrisCount); ++j) {
                  flt32_t t = IntersectTriangle(obj.tris[j], rayS);
                  has_obstacle = (t < light_dist);
                }
              }
              // if no obstacles found, sample color from tri (otherwise, apply ambient only)
              if (!has_obstacle) {
                // specular
                vec4_t reflect_dir = FSL_Vec4_Reflect(light_ray, point_norm); // light ray is inverted already!
                vec4_t eye_dir = FSL_Vec4_Inverse(ray.dir);
                flt32_t spec = FSL_F32_Pow(FSL_MAX(FSL_Vec4_Dot(reflect_dir, eye_dir), 0.0f), matShine);
                vec4_t spec_col = FSL_Vec4_Mul1(matSpec, spec);
                // diffuse
                vec4_t diff_col;
                if (diffNorms) {
                  if (diffNormsInterp) {
                    diff_col = FSL_Vec4_Mul(FSL_Vec4_Add(point_norm, FSL_Vec4_Set1(1.0f)), FSL_Vec4_Set(0.5f, 0.5f, 0.5f, 1.0f));
                  }
                  else {
                    diff_col = obj.tris[triI].col;
                  }
                }
                else {
                  diff_col = matDiff;
                }
                flt32_t light_coef = 1.0f / (lightConstCoef + (lightLinCoef + lightSqrCoef * light_dist) * light_dist);
                // diffuse/specular result
                col = FSL_Vec4_Add(col, diff_col);
                col = FSL_Vec4_Add(col, spec_col);
                col = FSL_Vec4_Mul1(col, light_coef * lambert);
                col = FSL_Vec4_Mul(col, lightColorDiff);
              }
            }
            // add ambient to result (always present)
            col = FSL_Vec4_Add(col, FSL_Vec4_Mul(matAmb, lightColorAmb));
          }
        }
      };
      // render
      #pragma omp parallel for schedule(dynamic)
      for (max_t y = 0; y < (max_t)scrHeight; ++y) {
        ray_t ray;
        ray.base = rayBase;
        flt32_t rayOffCnt = rayOffStepCoef - 1.0f;
        vec4_t colNorm = FSL_Vec4_Set1(255.0f / (rayOffCnt * rayOffCnt));
        vec4_t colMax = FSL_Vec4_Set1(255.0f);
        for (size_t x = 0; x < scrWidth; ++x) {
          vec4_t resCol = FSL_Vec4_SetZero();
          flt32_t rayOffY = rayOffStep;
          while (rayOffY < 1.0f) {
            flt32_t rayOffX = rayOffStep;
            while (rayOffX < 1.0f) {
              // generate primary ray
              flt32_t xF = static_cast<flt32_t>(x);
              flt32_t yF = static_cast<flt32_t>(y);
              flt32_t r_x = (2.0f * ((xF + rayOffX) / scrWidthF) - 1.0f) * aspect * scale;
              flt32_t r_y = (1.0f - 2.0f * (yF + rayOffY) / scrHeightF) * scale;
              ray.dir = FSL_Vec4_Normalize(FSL_Mat4_Mul4(invView, FSL_Vec4_Set(r_x, r_y, -1.0f, 0.0f)));
              // depth/occlusion
              flt32_t tnearS = INFINITY;
              // get color
              vec4_t col = colorBack;
              // check if light/camera is in front of object
              if (drawAbstract) {
                if (lightIsEye) {
                  tnearS = IntersectSphere(camera, ray);
                  if (tnearS != INFINITY) {
                    col = cameraColorDiff;
                  }
                }
                else {
                  tnearS = IntersectSphere(light, ray);
                  if (tnearS != INFINITY) {
                    col = lightColorDiff;
                  }
                }
              }
              objIntersect(objs[0], ray, tnearS, col);
              if (objMultiple) {
                for (size_t j = 1; j < objsCount; ++j) {
                  objIntersect(objs[j], ray, tnearS, col);
                }
              }
              resCol = FSL_Vec4_Add(resCol, col);
              rayOffX += rayOffStep;
            }
            rayOffY += rayOffStep;
          }
          // draw pixel
          resCol = FSL_Vec4_Mul(resCol, colNorm);
          resCol = FSL_Vec4_Min(resCol, colMax);
          uint8_t r = static_cast<uint8_t>(resCol.x);
          uint8_t g = static_cast<uint8_t>(resCol.y);
          uint8_t b = static_cast<uint8_t>(resCol.z);
          FastDraw(x, y, olc::Pixel(r, g, b));
        }
      }
      // draw UI (if visible)
      if (uiVisible) {
        ui.drawUIObjects();
        DrawString(5,  9, uiValueTypeStr);
        DrawString(5, 29, uiValueXStr);
        DrawString(5, 49, uiValueYStr);
        DrawString(5, 69, uiValueZStr);
      }

      return true;
    }

  private:
    // update user input
    void UpdateInput(flt32_t fElapsedTime) {
      // if UI visible, ignore input
      if (uiVisible) {
        return;
      }
      // calc move delta
      flt32_t look_delta = fElapsedTime * lookSpeed;
      if (GetKey(olc::Key::CTRL).bHeld) {
        look_delta *= lookBoost;
      }
      // get mouse delta
      flt32_t cursorDeltaX = look_delta * (GetKey(olc::Key::RIGHT).bHeld - GetKey(olc::Key::LEFT).bHeld);
      flt32_t cursorDeltaY = look_delta * (GetKey(olc::Key::DOWN ).bHeld - GetKey(olc::Key::UP  ).bHeld);
      // if mouse moved, update look vectors
      if (((cursorDeltaX > FSL_F32_EPSILON) || (cursorDeltaX < -FSL_F32_EPSILON)) || ((cursorDeltaY > FSL_F32_EPSILON) || (cursorDeltaY < -FSL_F32_EPSILON))) {
        // update yaw/pitch
        lookYaw   -= cursorDeltaX;
        lookPitch += cursorDeltaY;
        lookPitch = FSL_F32_Clamp(lookPitch, -89.95f, 89.95f);
        // update look vectors
        flt32_t yr = FLT32_DEG2RAD(lookYaw);
        flt32_t cy = FSL_F32_CosRad(yr);
        flt32_t sy = FSL_F32_SinRad(yr);
        flt32_t pr = FLT32_DEG2RAD(lookPitch);
        flt32_t cp = FSL_F32_CosRad(pr);
        flt32_t sp = FSL_F32_SinRad(pr);
        lookForward = FSL_Vec4_Normalize(FSL_Vec4_Set(
          cy * cp,
          sp,
          sy * cp,
          0.0f
        ));

        lookRight = FSL_Vec4_Normalize(FSL_Vec4_Cross(lookForward, FSL_VEC4_POS_Y));
        lookUp = FSL_Vec4_Normalize(FSL_Vec4_Cross(lookRight, lookForward));
      }
      // change eye to camera
      if (GetKey(olc::Key::C).bPressed) {
        eyePos = &camera.center;
        lightIsEye = false;
      }
      // change eye to light
      if (GetKey(olc::Key::L).bPressed) {
        eyePos = &light.center;
        lightIsEye = true;
      }
      // toggle diffuse color == normal (or normal at point)
      if (GetKey(olc::Key::N).bPressed) {
        if (diffNorms) {
          diffNorms = !diffNormsInterp;
          diffNormsInterp = !diffNormsInterp;
        }
        else {
          diffNorms = true;
        }
      }
      // toggle drawing of abstract objects (camera/light)
      if (GetKey(olc::Key::T).bPressed) {
        drawAbstract = !drawAbstract;
      }
      // toggle drawing of extra objects
      if (GetKey(olc::Key::M).bPressed) {
        objMultiple = !objMultiple;
      }
      // toggle light shadows
      if (GetKey(olc::Key::Y).bPressed) {
        lightShadows = !lightShadows;
      }
      // increase ray offset step coef (higher values will generate more rays, 2.0f minimum)
      if (GetKey(olc::Key::P).bPressed) {
        rayOffStepCoef += 1.0f;
        rayOffStepCoef = FSL_MIN(rayOffStepCoef, 6.0f);//FSL_MIN(rayOffStepCoef, 11.0f); // max before color shift
        rayOffStep = (1.0f / rayOffStepCoef);
      }
      // decrease ray offset step coef (higher values will generate more rays, 2.0f minimum)
      if (GetKey(olc::Key::O).bPressed) {
        rayOffStepCoef -= 1.0f;
        rayOffStepCoef = FSL_MAX(rayOffStepCoef, 2.0f);
        rayOffStep = (1.0f / rayOffStepCoef);
      }
      // position update
      vec4_t mov = FSL_Vec4_SetZero();
      flt32_t mov_delta = moveSpeed * fElapsedTime;
      if (GetKey(olc::Key::SHIFT).bHeld) {
        mov_delta *= moveBoost;
      }
      if (GetKey(olc::Key::D).bHeld) {
        mov = FSL_Vec4_Add(mov, lookRight);
      }
      if (GetKey(olc::Key::A).bHeld) {
        mov = FSL_Vec4_Sub(mov, lookRight);
      }
      if (GetKey(olc::Key::E).bHeld) {
        mov = FSL_Vec4_Add(mov, lookUp);
      }
      if (GetKey(olc::Key::Q).bHeld) {
        mov = FSL_Vec4_Sub(mov, lookUp);
      }
      if (GetKey(olc::Key::W).bHeld) {
        mov = FSL_Vec4_Sub(mov, lookForward);
      }
      if (GetKey(olc::Key::S).bHeld) {
        mov = FSL_Vec4_Add(mov, lookForward);
      }
      *eyePos = FSL_Vec4_Add(*eyePos, FSL_Vec4_Mul1(FSL_Vec4_Normalize(mov), mov_delta));
      // update fov
      const flt32_t fovDelta = 10.0f * fElapsedTime * ((int32_t)GetKey(olc::Key::NP_SUB).bHeld + -(int32_t)GetKey(olc::Key::NP_ADD).bHeld);
      if ((fovDelta > FSL_F32_EPSILON) || (fovDelta < -FSL_F32_EPSILON)) {
        fov += fovDelta;
        const flt32_t fov_y = FLT32_DEG2RAD(fov) / 2.0f;
        scale = FSL_F32_SinRad(fov_y) / FSL_F32_CosRad(fov_y);
      }
    }

    // update ui
    void UpdateUI(flt32_t fElapsedTime) {
      // check UI is visible
      if (!uiVisible) {
        bool ui_toggle = false;
        if (GetKey(olc::Key::K1).bPressed) {
          uiValueTypeStr = "Light Diffuse:";
          uiValueXStr = "R:";
          uiValueYStr = "G:";
          uiValueZStr = "B:";
          uiVisible = true;
          uiValue = &lightColorDiff;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K2).bPressed) {
          uiValueTypeStr = "Light Ambient:";
          uiValueXStr = "R:";
          uiValueYStr = "G:";
          uiValueZStr = "B:";
          uiVisible = true;
          uiValue = &lightColorAmb;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K3).bPressed) {
          uiValueTypeStr = "Material Ambient:";
          uiValueXStr = "R:";
          uiValueYStr = "G:";
          uiValueZStr = "B:";
          uiVisible = true;
          uiValue = &material.ambient;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K4).bPressed) {
          uiValueTypeStr = "Material Diffuse:";
          uiValueXStr = "R:";
          uiValueYStr = "G:";
          uiValueZStr = "B:";
          uiVisible = true;
          uiValue = &material.diffuse;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K5).bPressed) {
          uiValueTypeStr = "Material Specular:";
          uiValueXStr = "R:";
          uiValueYStr = "G:";
          uiValueZStr = "B:";
          uiVisible = true;
          uiValue = &material.specular;
          ui_toggle = true;
        }
        // TODO - Material Shininess
        if (GetKey(olc::Key::K7).bPressed) {
          uiValueTypeStr = "Transform Position:";
          uiValueXStr = "X:";
          uiValueYStr = "Y:";
          uiValueZStr = "Z:";
          uiVisible = true;
          uiValue = &objPosition;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K8).bPressed) {
          uiValueTypeStr = "Transform Scale:";
          uiValueXStr = "X:";
          uiValueYStr = "Y:";
          uiValueZStr = "Z:";
          uiVisible = true;
          uiValue = &objScale;
          ui_toggle = true;
        }
        if (GetKey(olc::Key::K9).bPressed) {
          uiValueTypeStr = "Transform Rotation:";
          uiValueXStr = "X:";
          uiValueYStr = "Y:";
          uiValueZStr = "Z:";
          uiVisible = true;
          uiValue = &objRotation;
          ui_toggle = true;
        }
        // if ui not toggled, return
        if (!ui_toggle) {
          return;
        }
        // set values
        char buf[11];
        FSL_SNPRINTF(buf, 11, "%f", uiValue->x);
        ui.setTxtFieldStr(0, buf);
        FSL_SNPRINTF(buf, 11, "%f", uiValue->y);
        ui.setTxtFieldStr(1, buf);
        FSL_SNPRINTF(buf, 11, "%f", uiValue->z);
        ui.setTxtFieldStr(2, buf);
      }
      if (GetKey(olc::Key::ESCAPE).bPressed) {
        uiVisible = false;
        return;
      }
      // this handles the update of all items
      ui.Update(fElapsedTime);
      // if enter pressed, update value
      if (GetKey(olc::Key::ENTER).bPressed) {
        std::string var;
        var = ui.getTxtFieldStr(0);
        if (!var.empty()) {
          uiValue->x = strtof(var.c_str(), NULL);
        }
        var = ui.getTxtFieldStr(1);
        if (!var.empty()) {
          uiValue->y = strtof(var.c_str(), NULL);
        }
        var = ui.getTxtFieldStr(2);
        if (!var.empty()) {
          uiValue->z = strtof(var.c_str(), NULL);
        }
        uiVisible = false;
      }
    }

    // transforms object
    void TransformObject() {
      // set obj transformation matrices
      mat4_t objModelPosition = FSL_Mat4_Translation(objPosition);
      mat4_t objModelScale = FSL_Mat4_Scale(objScale);
      mat4_t objModelInvScale = FSL_Mat4_Scale(FSL_Vec4_DivZero(FSL_Vec4_Set1(1.0f), objScale));
      mat4_t objModelRotation = FSL_Quat_ToMat4(FSL_Quat_InitEulerDeg(objRotation));//FSL_Mat4_Rotation(objRotation);
      mat4_t objModelNormal = FSL_Mat4_Mul(objModelRotation, FSL_Mat4_Transpose(objModelInvScale));
      mat4_t objModel = FSL_Mat4_Mul(objModelRotation, objModelScale);
      objModel = FSL_Mat4_Mul(objModelPosition, objModel);
      //transform objects
      for (size_t idx = 0; idx < objsCount; ++idx) {
        if (!objMultiple && idx) continue;
        // set obj transformation matrices for obj
        fsr_obj_t& objX = objs[idx];
        mat4_t objXModelPosition = FSL_Mat4_Translation(objX.transform.position);
        mat4_t objXModelScale = FSL_Mat4_Scale(objX.transform.scale);
        mat4_t objXModelInvScale = FSL_Mat4_Scale(FSL_Vec4_DivZero(FSL_Vec4_Set1(1.0f), objX.transform.scale));
        mat4_t objXModelRotation = FSL_Quat_ToMat4(FSL_Quat_InitEulerDeg(objX.transform.rotation));//FSL_Mat4_Rotation(obj1.transform.rotation);
        mat4_t objXModelNormal = FSL_Mat4_Mul(objXModelRotation, FSL_Mat4_Transpose(objXModelInvScale));
        objXModelNormal = FSL_Mat4_Mul(objXModelNormal, objModelNormal);
        mat4_t objXModel = FSL_Mat4_Mul(objXModelRotation, objXModelScale);
        objXModel = FSL_Mat4_Mul(objXModelPosition, objXModel);
        objXModel = FSL_Mat4_Mul(objXModel, objModel);
        // transform object
        vec4_t minB = FSL_Vec4_Set( INFINITY,  INFINITY,  INFINITY, 1.0f);
        vec4_t maxB = FSL_Vec4_Set(-INFINITY, -INFINITY, -INFINITY, 1.0f);
        //#pragma omp parallel for schedule(dynamic)
        for (max_t i = 0; i < (max_t)objTrisCount; ++i) {
          tri_t& tri = objX.tris[i];
          tri = objTris[i].Transform(objXModel, objXModelNormal);
          // update hull
          //#pragma omp critical
          {
            minB = FSL_Vec4_Min(minB, tri.a);
            maxB = FSL_Vec4_Max(maxB, tri.a);
            minB = FSL_Vec4_Min(minB, tri.b);
            maxB = FSL_Vec4_Max(maxB, tri.b);
            minB = FSL_Vec4_Min(minB, tri.c);
            maxB = FSL_Vec4_Max(maxB, tri.c);
          }
        }
        objX.hull.min = minB;
        objX.hull.max = maxB;
      }
    }

    olc::UI_CONTAINER ui;

    fsr_obj_t objs[4] = {
      {
        {},
        nullptr,
        0,
        {
          FSL_VEC4_INIT(0.0f, 0.0f, 0.0f, 0.0f),
          FSL_VEC4_INIT(0.0f, 0.0f, 0.0f, 0.0f),
          FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 0.0f)
        },
        {
          FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 1.0f),
          FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 1.0f),
          FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 1.0f),
          1.0f
        }
      },
      {
        {},
        nullptr,
        0,
        {
          FSL_VEC4_INIT(0.0f, 1.0f, 3.0f, 0.0f),
          FSL_VEC4_INIT(50.0f, 20.0f, 90.0f, 0.0f),
          FSL_VEC4_INIT(0.5f, 2.0f, 1.0f, 0.0f)
        },
        {
          FSL_VEC4_INIT(0.4f, 0.2f, 0.0f, 1.0f),
          FSL_VEC4_INIT(0.4f, 0.4f, 0.8f, 1.0f),
          FSL_VEC4_INIT(0.1f, 0.2f, 0.4f, 1.0f),
          9.0f
        }
      },
      {
        {},
        nullptr,
        0,
        {
          FSL_VEC4_INIT(2.0f, -2.0f, 0.0f, 0.0f),
          FSL_VEC4_INIT(120.0f, -70.0f, -30.0f, 0.0f),
          FSL_VEC4_INIT(1.0f, 3.0f, 2.0f, 0.0f)
        },
        {
          FSL_VEC4_INIT(0.0f, 0.2f, 0.4f, 1.0f),
          FSL_VEC4_INIT(0.8f, 0.4f, 0.2f, 1.0f),
          FSL_VEC4_INIT(0.4f, 0.5f, 1.0f, 1.0f),
          18.0f
        }
      },
      {
        {},
        nullptr,
        0,
        {
          FSL_VEC4_INIT(-4.0f, 2.0f, 0.0f, 0.0f),
          FSL_VEC4_INIT(40.0f, 20.0f, 0.0f, 0.0f),
          FSL_VEC4_INIT(0.4f, 2.0f, 4.2f, 0.0f)
        },
        {
          FSL_VEC4_INIT(0.2f, 0.3f, 0.2f, 1.0f),
          FSL_VEC4_INIT(0.3f, 0.7f, 0.5f, 1.0f),
          FSL_VEC4_INIT(0.1f, 0.2f, 0.3f, 1.0f),
          6.66f
        }
      }
    };
    size_t objsCount = 4;
    bool   objMultiple = false;

    material_t  material     = {
      FSL_VEC4_INIT(0.0f, 0.1f, 0.2f, 1.0f),
      FSL_VEC4_INIT(0.2f, 0.8f, 1.0f, 1.0f),
      FSL_VEC4_INIT(0.0f, 0.25f, 0.5f, 1.0f),
      21.0f
    };

    sphere_t camera          = {
      FSL_VEC4_INIT( 0.0f,  0.0f, -3.0f, 1.0f),
      0.05f
    };
    vec4_t   cameraColorDiff = FSL_VEC4_INIT(0.4f, 0.2f, 0.0f, 1.0f);

    vec4_t*  eyePos;         //= nullptr;
    flt32_t  moveSpeed       = 1.0f;
    flt32_t  moveBoost       = 5.0f;

    vec4_t   colorBack       = FSL_VEC4_INIT(0.2f, 0.3f, 0.3f, 1.0f);
    bool     diffNorms       = false;
    bool     diffNormsInterp = false;

    flt32_t  rayOffStepCoef  = 3.0f;
    flt32_t  rayOffStep      = (1.0f / rayOffStepCoef);

    sphere_t light           = {
      FSL_VEC4_INIT( 0.0f,  0.0f, -5.0f, 1.0f),
      0.05f
    };
    vec4_t   lightColorDiff  = FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 1.0f);
    vec4_t   lightColorAmb   = FSL_VEC4_INIT(0.0f, 0.0f, 0.0f, 1.0f);
    flt32_t  lightConstCoef  = 1.0f;
    flt32_t  lightLinCoef    = 0.09f;
    flt32_t  lightSqrCoef    = 0.032f;
    bool     lightIsEye      = false;
    bool     lightShadows    = true;

    bool     drawAbstract    = true;

    vec4_t   lookForward     = FSL_VEC4_INIT( 0.0f,  0.0f, -1.0f, 0.0f);
    vec4_t   lookUp          = FSL_VEC4_INIT( 0.0f,  1.0f,  0.0f, 0.0f);
    vec4_t   lookRight       = FSL_VEC4_INIT( 1.0f,  0.0f,  0.0f, 0.0f);
    flt32_t  lookYaw         = -90.0f;
    flt32_t  lookPitch       = 0.0f;
    flt32_t  lookSpeed       = 30.0f;
    flt32_t  lookBoost       = 5.0f;

    vec4_t   objPosition     = FSL_VEC4_ZERO;
    vec4_t   objRotation     = FSL_VEC4_ZERO;
    vec4_t   objScale        = FSL_VEC4_INIT(1.0f, 1.0f, 1.0f, 0.0f);
    tri_t*   objTris;        //= nullptr;
    size_t   objTrisCount;   //= 0;
    aabb_t   objHull;        //= { };

    vec4_t*  uiValue;        //= nullptr;
    cstr_t   uiValueTypeStr; //= nullptr;
    cstr_t   uiValueXStr;    //= nullptr;
    cstr_t   uiValueYStr;    //= nullptr;
    cstr_t   uiValueZStr;    //= nullptr;
    bool     uiVisible       = false;

    size_t   scrWidth;       //= 0;
    size_t   scrHeight;      //= 0;
    flt32_t  scrWidthF;      //= 0.0f;
    flt32_t  scrHeightF;     //= 0.0f;
    flt32_t  fov;            //= FLT32_DEG2RAD(45.0f) / 2.0f;
    flt32_t  scale;          //= FSL_F32_Sin(fov) / FSL_F32_Cos(fov);
    flt32_t  aspect;         //= scrWidthF / scrHeightF;
};

int32_t main(int32_t argc, char** argv) {
  if (FSL_Mem_Init()) {
    return 1;
  }

  // modes:
  // 0:  320×180 resolution, 4×4 scaling
  // 1:  427×240 resolution, 3×3 scaling
  // 2:  640×360 resolution, 2×2 scaling
  // 3: 1280×720 resolution, 1×1 scaling
  uint32_t mode = 1;
  if (argc > 1) {
    if (argv[1][0] == '-' && argv[1][2] == '\0') {
      mode = static_cast<uint32_t>(argv[1][1] - '0');
      if (argc > 2) {
        modelPath = argv[2];
      }
    }
    else {
      modelPath = argv[1];
    }
  }

  FSRay fsr;
  switch(mode) {
    case 0:
      if (!fsr.Construct(320, 180, 4, 4)) {
        return 3;
      }
      break;
    case 1:
      if (!fsr.Construct(427, 240, 3, 3)) {
        return 3;
      }
      break;
    case 2:
      if (!fsr.Construct(640, 360, 2, 2)) {
        return 3;
      }
      break;
    case 3:
      if (!fsr.Construct(1280, 720, 1, 1)) {
        return 3;
      }
      break;
    default:
      return 2; // invalid mode!
  }

  fsr.Start();
  return 0;
}